#define top_dirPin  9
#define top_stepperPin  8
#define bottom_dirPin  11
#define bottom_stepperPin  10
#define top_interrupt_Pin 3 //interrupt 0
#define bottom_interrupt_Pin 7 //interrupt 4
//this arduino board interrup 1:pin 2 not working.
#define magnet_relay_Pin 4 //relay high active
volatile int top_zero_Flag = LOW;
volatile int bottom_zero_Flag = LOW;
//int updown = LOW;
int step_size = 63/200 ; //calibration 63mm per 200 steps
int loop_index = 0 ;
int i = 0 ;
int j = 0 ;
int k = 0 ;

void setup ()
{
pinMode(top_dirPin, OUTPUT);
pinMode(top_stepperPin, OUTPUT);
pinMode(bottom_dirPin, OUTPUT);
pinMode(bottom_stepperPin, OUTPUT);
pinMode(magnet_relay_Pin, OUTPUT);
digitalWrite (top_dirPin, HIGH);
digitalWrite (bottom_dirPin, HIGH);
digitalWrite (magnet_relay_Pin, LOW);
attachInterrupt (digitalPinToInterrupt (top_interrupt_Pin), top_zero_Detection, RISING);
attachInterrupt (digitalPinToInterrupt (bottom_interrupt_Pin), bottom_zero_Detection, RISING);
top_zero ();
bottom_zero ();
}

void top_zero_Detection () 
{
//  updown = ! updown ;
//  if (updown == HIGH )
  top_zero_Flag = HIGH ;
}

void bottom_zero_Detection () 
{
  bottom_zero_Flag = HIGH ;
}

void top_move (int steps)
{
  for (int i=0 ; i< steps; i++)
  {
    digitalWrite(top_stepperPin, HIGH);
    delayMicroseconds(800);
    digitalWrite(top_stepperPin, LOW);
    delayMicroseconds(800);
    delay (1); 
  } 
}

void top_move_forward (int fsteps)
{
  int steps = 0; steps = fsteps;
  digitalWrite (top_dirPin,HIGH);
  top_move (steps);
}

void top_move_backward (int bsteps)
{
  int steps = 0 ; steps = bsteps;
  digitalWrite (top_dirPin,LOW);
  top_move (steps);
}

void top_zero ()
{
  top_zero_Flag = LOW;
  attachInterrupt (digitalPinToInterrupt (top_interrupt_Pin), top_zero_Detection, RISING);
  top_zero_Flag = LOW;
  while (top_zero_Flag == LOW)
  {
    top_move_backward (1);
  } 
  detachInterrupt (digitalPinToInterrupt (top_interrupt_Pin));
  digitalWrite (top_dirPin, HIGH);
  top_move_forward (13); //top_zero_position shift
  top_zero_Flag = LOW ;
}

void bottom_move (int steps)
{
  for (int i=0 ; i< steps; i++)
  {
    digitalWrite(bottom_stepperPin, HIGH);
    delayMicroseconds(800);
    digitalWrite(bottom_stepperPin, LOW);
    delayMicroseconds(800);
    delay (1); 
  }
}

void bottom_move_forward (int fsteps)
{
  int steps = 0; steps = fsteps;
  digitalWrite (bottom_dirPin,HIGH);
  bottom_move (steps);
}

void bottom_move_backward (int bsteps)
{
  int steps = 0; steps = bsteps;
  digitalWrite (bottom_dirPin,LOW);
  bottom_move (steps);
}

void bottom_zero ()
{
  bottom_zero_Flag = LOW ;
  attachInterrupt (digitalPinToInterrupt (bottom_interrupt_Pin), bottom_zero_Detection, RISING);
  bottom_zero_Flag = LOW;
  while (bottom_zero_Flag == LOW)
  {
    bottom_move_backward (1);
  }
  detachInterrupt (digitalPinToInterrupt (bottom_interrupt_Pin));
  digitalWrite (bottom_dirPin, HIGH);
  bottom_move_forward (16); //bottom_zero_position shift
  bottom_zero_Flag = LOW ;
}

void magnet_on ()
{
  digitalWrite (magnet_relay_Pin, HIGH);
}

void magnet_off ()
{
  digitalWrite (magnet_relay_Pin, LOW);
}

void loop()
{
  if (loop_index < 5 )
  {
 // move stone from (0,4) to (5,2) position 
//from (0,0) to (0,4)
  top_zero () ;
  delay (1000);
  bottom_zero ();
  delay (1000);
  top_move_forward (254); //from (0,0) to (0,4) ; (4*20mm)*(200steps/63mm)=190
//
  magnet_on ();
  delay (2000);
//move by two steps ;from (0,4) to (3,2), then from (3,2) to (5,2);
  bottom_move_forward (222) ;// from (0,4) to (3,4); (3+0.5)*20mm*(200steps/63mm)=222
  delay (1000);
  top_move_backward (127); //from (3,4) to (3,2) ; (2*20mm)
  delay (1000);
  bottom_move_forward (127) ; //from (3,2) to (5,2) ; (2*20mm)
  magnet_off ();
  delay (2000);
//return to zero position
  bottom_zero ();
  top_zero ();
  delay (5000);
// move stone from (5,2) to (0,4)
// go to (5,2)
  bottom_move_forward (350);// from (0,0) to (5,0); (5+0.5)*20mm;
  delay (1000);
  top_move_forward (127); //from (5,0) to (5,2); (2*20mm);
  delay (1000);
  magnet_on ();
  delay (2000);
//move back from (5,2) to (0,4)
  bottom_move_backward (127); // from (5,2) to (3,2); (2*20mm);
  delay (1000);
  top_move_forward (127); // from (3,2) to (3,4) ; (2*20mm);
  delay (1000);
  bottom_move_backward (222); // from (3,4) to (0,4) ; (3+0.5)*20mm
  delay (1000);
  magnet_off ();
  delay (2000); 
//return to zero position
  bottom_zero ();
  delay (1000);
  top_zero ();
  delay (1000);
  loop_index = loop_index + 1 ;
  }
}


