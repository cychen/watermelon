
casex = 20 ; casey = 34 ; casez = 11.3 ;

module LM6UUhole ()
{
  x = 2; y = 0 ; z = 0 ; R = 6.25 ;     
    translate ([-x,y,z])
      cylinder (h=casez+0.2,r=R,center=true);
}

module earcut ()
{
  x = 4 ; y = 27 ; z = 0 ;
  for ( j = [y,-y] ) {
    translate ([-x,j,z])
    cube ([casex,casey,casez],center=true);
  }
}
 
module screwhole ()
{
  x = 8; y = 13.5 ; z = 0 ; R = 1.5 ;     
    for ( j = [y, -y] ) {
      translate ([x,j,z]) rotate ([0,90,0])
      cylinder (h=casez+0.2,r=R,center=true);
    }
}

module LM6UUcase ()
{    
  difference () {    
    color ("blue")
    cube ([casex,casey,casez],center=true); 
    LM6UUhole (); 
    earcut () ; 
    screwhole ();     
  }
}


translate ([0,0,casez/2]) 
LM6UUcase ();
