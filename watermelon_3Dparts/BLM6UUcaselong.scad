
casex = 20 ; casey = 34 ; casez = 30.3 ;

module LM6UUhole ()
{
  x = 2; y = 0 ; z = 0 ; R = 6.0 ;     
    translate ([-x,y,z])
      cylinder (h=casez+0.2,r=R,center=true);
}

module earcut ()
{
  x = 4 ; y = 27 ; z = 0 ;
  for ( j = [y,-y] ) {
    translate ([-x,j,z])
    cube ([casex,casey,casez],center=true);
  }
}
 
module screwhole ()
{
  x = 8; y = 13.5 ; z = 9.5 ; R = 1.5 ;     
    for ( j = [y, -y] ) {
      for ( k = [z, -z] ) {
        translate ([x,j,k]) rotate ([0,90,0])
        cylinder (h=casez+0.2,r=R,center=true);
      }
    }
}

module slot ()
{
  x= 9 ; y = 0; z = 0 ; 
    translate ([-x,y,z])
    cube ([4,2,casez+0.2],center=true);
}

module LM6UUcase ()
{    
  difference () {    
    color ("blue")
    cube ([casex,casey,casez],center=true); 
    LM6UUhole (); 
    earcut () ; 
    screwhole ();
    slot ();     
  }
}

rotate ([0,90,0])
translate ([-casex/2,0,0]) 
LM6UUcase ();
