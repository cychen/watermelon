
holderx = 62.30 ; holdery = 42.30 ; holderz = 8 ;

module BMotorhole ()
{
  x = 0; y = 0 ; z = 0 ; R = 11.0 ;     
    translate ([x,y,z])
      cylinder (h=holderz+0.2,r=R,center=true);
}

module earcut ()
{
  x = 52.30 ; y = 5 ; z = 0 ;
  for ( i = [x,-x] ) {
    translate ([i,-y,z])
    cube ([holderx,holdery,holderz],center=true);
  }
}
 
module earscrewhole ()
{
  x = 26.15; y = 18.65 ; z = 0 ; R = 1.5 ;     
    for ( i = [x, -x] ) {
      translate ([i,y,z]) rotate ([90,0,0])
      cylinder (h=holderz+0.2,r=R,center=true);
    }
}

module motorscrewhole ()
{
  x = 15.5 ; y = 15.5 ; z = 0 ; Rs = 2.0 ; Rb = 3.0;
  for ( i = [x, -x] ) {
    for ( j = [y, -y ] ) {
      translate ([i,j,z]) {
        union () {
          translate ([0,0,5]) 
            cylinder (h=holderz +0.2, r=Rb,center=true);
          cylinder (h=holderz+0.2,r=Rs,center=true);
        }
      }
    }
  }
}  

module BMotorholder ()
{    
  difference () {    
    color ("blue")
    cube ([holderx,holdery,holderz],center=true); 
    BMotorhole (); 
    earcut () ; 
    earscrewhole (); 
    motorscrewhole ();    
  }
}


translate ([0,0,holderz/2]) 
BMotorholder ();
