
casex = 20 ; casey = 27 ; casez = 11.3 ;

module LM6UUhole ()
{
  x = 0; y = 3.8 ; z = 0 ; R = 6.0 ;     
    translate ([x,y,z])
      cylinder (h=casez+0.2,r=R,center=true);
}
 
module screwhole ()
{
  x = 0 ; y1 = 9.5 ; y2 = 11.65 ; z = 0 ; R = 1.5 ;     
  translate ([x,-y1,z])
    cylinder (h=casez+0.2,r=R,center=true);
  translate ([x, y2, z ]) rotate ([90,0,0])
    cylinder (h=4,r=R,center=true);
}

module LM6UUcase ()
{    
  difference () {    
    color ("blue")
    cube ([casex,casey,casez],center=true); 
    LM6UUhole ();  
    screwhole ();     
  }
}


translate ([0,0,casez/2]) 
LM6UUcase ();
