
basex = 57 ; basey = 42.5 ; basez = 13 ;

module sidecut ()
{
  x = 3; y = 30.15 ; z = 0 ;     
  for (j = [y, -y]) {
    translate ([-x,j,z])
    cube ([basex,basey,basez],center=true);
  }
}

module topcut ()
{
  x=3; y =0 ; z =3;
  translate ([-x,y,z])
    cube ([basex,basey,basez],center=true);
}

module wall ()
{
  x = 18.50; y = 7.4 ; z=0.5;
  wallx = 20 ; wally = 3 ;wallz =8 ; R=1.75;
  for ( j = [y,-y]) {
    translate ([-x,j,z]) {
      difference () {
        cube ([wallx,wally,wallz],center=true);
        translate ([-2.5,0,0]) rotate ([90,0,0])
          cylinder (h=wally+0.2,r=R,center=true);
      }
    }
  }
}

module screwhole ()
{
  x = 27 ; y = 15.5 ; z = 2.15 ; R = 1.75 ;     
  for (j = [y,-y]) {
    translate ([x,j,z]) rotate ([0,90,0])
      cylinder (h=3+0.2,r=R,center=true);
  }
}

module TMotorLM6UUbase ()
{    
  union () {
    difference () {    
      color ("blue")
      cube ([basex,basey,basez],center=true); 
      sidecut ();  
      topcut ();
      screwhole ();     
    }
    wall ();
  }
}


translate ([0,0,basez/2]) 
TMotorLM6UUbase ();
