
basex = 54 ; basey = 31 ; basez = 3 ;
basecutx = 34; basecuty= 15.50; basecutz = 3;
motorpanelx = 3; motorpanely = 42.5 ; motorpanelz=13;
bearingx = 20 ; bearingy= 11.3; bearingz = 30; bearingR=6;

module base ()
{ 
    cube ([basex,basey,basez],center=true);
}

module basecut ()
{
  x = 10; y = 7.75 ; z = 0 ;     
  translate ([x,-y,z])
    cube ([basecutx,basecuty,basecutz],center=true);
}

module motorpanel ()
{
  x = 28.5; py = 8.75 ; sy = 15.5 ; pz =5; sz = 2.15 ; R=1.75;
  translate ([x,py,pz])
  difference () {
    cube ([motorpanelx,motorpanely,motorpanelz],center=true);
    for ( j = [sy,-sy]) {
      translate ([0,j,sz]) rotate ([0,90,0])
      cylinder (h=3+0.2,r=R,center=true);   
    }
  }  
}

module bearingmount ()
{
  x = 17; y = 9.85 ; z= 13.5;
  for ( j = [y,-y]) {
    translate ([-x,j,z])
    difference () {
      cube ([bearingx,bearingy,bearingz],center=true);
      translate ([0,0,5.3]) 
      rotate ([90,0,0]) 
      cylinder (h=bearingy+0.2, r=bearingR+0.25,center=true);
    }
  }
}

module bearingmountscrewhole ()
{
    x = 17 ; y = 9.85 ; z = 24.8 ; 
    translate ([-x,y,z])
    cylinder (h=8,r=1.5,center=true);
}

module TMotorLM6UUbase ()
{ 
  color ("blue")  
  difference () {    
    union () {
      base ();
      motorpanel ();
      bearingmount ();
    } 
    basecut ();       
    bearingmountscrewhole ();
  }
}


translate ([0,0,basez/2]) 
TMotorLM6UUbase ();
