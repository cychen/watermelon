
standx = 32 ; standy = 28 ; standz = 8 ;

module Rodhole ()
{
  x = 6; y = 0 ; z = 0 ; R = 3 ;     
    translate ([x,y,z])
      cylinder (h=standz+0.2,r=R,center=true);
}

module earcut ()
{
  x = 3 ; y = 20 ; z = 0 ;
  for ( j = [y,-y] ) {
    translate ([x,j,z])
    cube ([standx,standy,standz],center=true);
  }
}
 
module earscrewhole ()
{
  x = 0; y = 10 ; z = 0 ; R = 2 ;     
    for ( j = [y, -y] ) {
      translate ([x,j,z]) rotate ([0,90,0])
      cylinder (h=standx+0.2,r=R,center=true);
    }
}

module rodscrewhole ()
{
  x = 12 ; y = 0 ; z = 0 ; R = 1.5 ; 
  translate ([x,y,z]) rotate ([90,0,0])
    cylinder (h=standy +0.2, r=R,center=true);
}  

module rodcut ()
{
    x = 12 ; y = 0 ; z = 0 ;
    translate ([x,y,z]) 
      cube ([8,2,standz+0.2],center=true);
}

module TRodstand ()
{    
  difference () {    
    color ("blue")
    cube ([standx,standy,standz],center=true); 
    Rodhole (); 
    earcut () ; 
    earscrewhole (); 
    rodscrewhole (); 
    rodcut ();   
  }
}


translate ([0,0,standz/2]) 
TRodstand ();
